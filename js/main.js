$(function(){
    var json_url = $('#json_url').attr('href');
    $.get(json_url, function(data){
        if(data.location.tz){

          var local_time = new Date( new Date().getTime() + data.location.tz_offset * 1000).toUTCString().replace( / GMT$/, "" ).substring(17,22);
          $("#local_time").html(local_time);
          $("#time_zone").html(data.location.tz);
        };

        if(data.location.place){
          $("#location_name").html(data.location.place);
        };

        if(data.location.weather){
          $("#weather_condition").html(data.location.weather.condition_text);
          $("#weather_temperature").html(data.location.weather.temperature_outside);
        }


        if(data.lastfm){
          $("#title").html(data.lastfm.song_title);
          $("#artist").html(data.lastfm.artist);
        };

        if(data.bookwyrm){
          $("#bookwyrm_update").html(data.bookwyrm.last_update);
        };

    });
});
